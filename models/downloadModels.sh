#!/bin/bash
Black='\033[0;30m'
DarkGray='\033[1;30m'
RED='\033[0;31m'
LightRed='\033[1;31m'
Green='\033[0;32m'
LightGreen='\033[1;32m'
BrownOrange='\033[0;33m'
Yellow='\033[1;33m'
Blue='\033[0;34m'
LightBlue='\033[1;34m'
Purple='\033[0;35m'
LightPurple='\033[1;35m'
Cyan='\033[0;36m'
LightCyan='\033[1;36m'
LightGray='\033[0;37m'
White='\033[1;37m'
NC='\033[0m'

echo -e "${LightGreen}Downloading AlexNet...${NC}"
wget -O AlexNet/bvlc_alexnet.caffemodel http://dl.caffe.berkeleyvision.org/bvlc_alexnet.caffemodel

echo -e "${LightBlue}Downloading GoogLeNet...${NC}"
wget -O GoogLeNet/bvlc_googlenet.caffemodel http://dl.caffe.berkeleyvision.org/bvlc_googlenet.caffemodel

echo -e "${LightCyan}Downloading MobileNet...${NC}"
wget -O MobileNet/mobilenet.caffemodel https://github.com/shicai/MobileNet-Caffe/raw/master/mobilenet.caffemodel

echo -e "${LightPurple}Downloading SqueezeNet...${NC}"
wget -O SqueezeNet/squeezenet_v1_0.caffemodel https://github.com/DeepScale/SqueezeNet/raw/master/SqueezeNet_v1.0/squeezenet_v1.0.caffemodel
wget -O SqueezeNet/squeezenet_v1_1.caffemodel https://github.com/DeepScale/SqueezeNet/raw/master/SqueezeNet_v1.1/squeezenet_v1.1.caffemodel

echo -e "${LightRed}...done with downloads${NC}"
