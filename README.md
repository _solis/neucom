# Deployment of Pretrained Neural Network to Embedded Platform

Final Winter Semester 2018/19 project for Neurocomputing taught by Prof. Dr.-Ing. Andreas König under Lehrstuhl Integrierte Sensorsysteme at Tchnische Universität Kaiserslautern

The main objective of this project is to deploy a pretrained Neural Network onto an embedded device.  For this case, it will be a Raspberry Pi 3 Model B Rev 1.2, but can also run on a PC given that the requirements (below) are also met. 

Networks chosen and used are as follows:
- AlexNet
- GoogLeNet
- MobileNet
- SqueezeNet

Models were trained offline through ImageNet. Read each repository for more details on training. Depoloyment uses ImageNet's synset_word.txt and those pretrained models.
## Direcotry Structure
```
├── images                  # Contains all images to be classified
├── models                  # Holds all models and deploy.prototxt for each
├── presentation            # Final presentation as editable .odb and and final .pdf
├── results                 # Previous results recorded
├── scripts                 # Bash scripts that run individual or all networks. They execute deploy.py
└── README.md
```
## Requirements
Directory and file structure is crucial for scripts to run. Keep those intact.

### Pretrained Models
GoogLeNet and AlexNet models from [BVLC](https://github.com/BVLC/caffe/tree/master/models), SqueezeNet model from [DeepScale](https://github.com/DeepScale/SqueezeNet), MobileNet model from [shicai](https://github.com/shicai/MobileNet-Caffe)

All models should be placed under models/{Network name}

For convenience of downloading models, /models/downloadModels.sh bash script has been created.
- Different models can be used, but user needs to change scripts to reference those models.

### Images

All scripts are hardcoded to read ONLY 4 images for classification, they are:
- diver.jpg
- dog.jpg
- p51.jpg
- kitchen.jpg

It is assumed this images exist under images/

Modifications to scripts can be done to support more than these 4 images

### OpenCV
It is assumed OpenCV 3.0+ is installed in the running computer. If this is not the case, a script called "installOpenCV.sh" has been written for this purpose.  This is an absolute must as OpenCV APIs support the Caffe framework models.

## Usage
"deploy.py" code is a modified version from article [Deep learning on the Raspberry Pi with OpenCV](https://www.pyimagesearch.com/2017/10/02/deep-learning-on-the-raspberry-pi-with-opencv/)

The code takes in the image to be classified, the prototxt for deployment, the pretrained Caffe model, and its labels. Dependent on the model name, the script will determine the proper crop and normalization params required for the model.

Options for deploy.py: > [-h] -i IMAGE -p PROTOTXT -m MODEL -l LABELS -d DISPLAY

Bash shell scripts were written to conveniently deploy each network: scripts/runAll.sh
- Deploys all pretrained networks and saves results to results/
- Depending on the system running the scripts, (ARM/Intel), scripts will save results with prefix "PC" or "RPi"
- All other shell scripts run deploy.py as stand alone and do not save results in a text file. They are console output only

## Results Output Example
```
------- SQUEEZENET VERSION 1.0 -------
P 51 Test
[INFO] loading model...
[INFO] model load time: 0.068891 seconds
[INFO] classification time: 0.79164 seconds
[INFO] total time: 0.86053 seconds
[INFO] 1. label: warplane, probability: 70.11%
[INFO] 2. label: wing, probability: 24.67%
[INFO] 3. label: airliner, probability: 4.10%
[INFO] 4. label: airship, probability: 0.54%
[INFO] 5. label: space shuttle, probability: 0.25%
```


