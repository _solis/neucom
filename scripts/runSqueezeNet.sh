#!/bin/bash
MODEL="../models/SqueezeNet"
IMAGE="../images"

echo ------- SQUEEZENET VERSION 1.0 -------
echo P 51 Test
python3 deploy.py                           \
--prototxt $MODEL/deploy_v1_0.prototxt      \
--model $MODEL/squeezenet_v1_0.caffemodel   \
--labels $IMAGE/synset_words.txt            \
--image $IMAGE/p51.jpg

echo -e "\nBorder Collie Test"
python3 deploy.py                           \
--prototxt $MODEL/deploy_v1_0.prototxt      \
--model $MODEL/squeezenet_v1_0.caffemodel   \
--labels $IMAGE/synset_words.txt            \
--image $IMAGE/dog.jpg

echo -e "\nKitchen Test"
python3 deploy.py                           \
--prototxt $MODEL/deploy_v1_0.prototxt      \
--model $MODEL/squeezenet_v1_0.caffemodel   \
--labels $IMAGE/synset_words.txt            \
--image $IMAGE/kitchen.jpg

echo -e "\nDiver Test"
python3 deploy.py                           \
--prototxt $MODEL/deploy_v1_0.prototxt      \
--model $MODEL/squeezenet_v1_0.caffemodel   \
--labels $IMAGE/synset_words.txt            \
--image $IMAGE/diver.jpg

echo ---------------------------------------------------
echo ------- SQUEEZENET VERSION 1.1 -------
echo P 51 Test
python3 deploy.py                           \
--prototxt $MODEL/deploy_v1_1.prototxt      \
--model $MODEL/squeezenet_v1_1.caffemodel   \
--labels $IMAGE/synset_words.txt            \
--image $IMAGE/p51.jpg

echo -e "\nBorder Collie Test"
python3 deploy.py                           \
--prototxt $MODEL/deploy_v1_1.prototxt      \
--model $MODEL/squeezenet_v1_1.caffemodel   \
--labels $IMAGE/synset_words.txt            \
--image $IMAGE/dog.jpg

echo -e "\nKitchen Test"
python3 deploy.py                           \
--prototxt $MODEL/deploy_v1_1.prototxt      \
--model $MODEL/squeezenet_v1_1.caffemodel   \
--labels $IMAGE/synset_words.txt            \
--image $IMAGE/kitchen.jpg

echo -e "\nDiver Test"
python3 deploy.py                           \
--prototxt $MODEL/deploy_v1_1.prototxt      \
--model $MODEL/squeezenet_v1_1.caffemodel   \
--labels $IMAGE/synset_words.txt            \
--image $IMAGE/diver.jpg