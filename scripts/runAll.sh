#!/bin/bash
OUTPUT="../results"
MODEL="$(cat /proc/cpuinfo | grep 'model name')"
if [[ "$MODEL" =~ "ARM" ]]; 
then
    sudo nice -n -15 bash runSqueezeNet.sh > $OUTPUT/RpiSqueezeNet.txt
    sudo nice -n -15 bash runGoogLeNet.sh > $OUTPUT/RpiGoogLeNet.txt
    sudo nice -n -15 bash runMobileNet.sh > $OUTPUT/RpiMobileNet.txt
    sudo nice -n -15 bash runAlexNet.sh > $OUTPUT/RpiAlexNet.txt
else 
    sudo nice -n -15 bash runSqueezeNet.sh > $OUTPUT/PCSqueezeNet.txt
    sudo nice -n -15 bash runGoogLeNet.sh > $OUTPUT/PCGoogLeNet.txt
    sudo nice -n -15 bash runMobileNet.sh > $OUTPUT/PCMobileNet.txt
    sudo nice -n -15 bash runAlexNet.sh > $OUTPUT/PCAlexNet.txt
fi

