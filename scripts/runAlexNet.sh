#!/bin/bash
MODEL="../models/AlexNet"
IMAGE="../images"

echo ------- ALEXNET -------
echo P 51 Test
python3 deploy.py                         \
--prototxt $MODEL/deploy.prototxt         \
--model $MODEL/bvlc_alexnet.caffemodel    \
--labels $IMAGE/synset_words.txt          \
--image $IMAGE/p51.jpg

echo -e "\nBorder Collie Test"
python3 deploy.py                         \
--prototxt $MODEL/deploy.prototxt         \
--model $MODEL/bvlc_alexnet.caffemodel    \
--labels $IMAGE/synset_words.txt          \
--image $IMAGE/dog.jpg

echo -e "\nKitchen Test"
python3 deploy.py                         \
--prototxt $MODEL/deploy.prototxt         \
--model $MODEL/bvlc_alexnet.caffemodel    \
--labels $IMAGE/synset_words.txt          \
--image $IMAGE/kitchen.jpg

echo -e "\nDiver Test"
python3 deploy.py                         \
--prototxt $MODEL/deploy.prototxt         \
--model $MODEL/bvlc_alexnet.caffemodel    \
--labels $IMAGE/synset_words.txt          \
--image $IMAGE/diver.jpg
