# Import packages
import numpy as np
import argparse
import time
import cv2

# Construct the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
                  help="path to input images")
ap.add_argument("-p", "--prototxt", required=True,
                  help="path to Caffe deply prototxt file")
ap.add_argument("-m", "--model", required=True,
                  help="path to Caffe pre-trained model")
ap.add_argument("-l", "--labels", required=True,
                  help="path to ImageNet labels")
ap.add_argument("-d", "--display", type=int, default=-1,
                  help="whether or not frames should be displayed")
args = vars(ap.parse_args())

# Load class labels
rows = open(args["labels"]).read().strip().split("\n")
classes = [r[r.find(" ") + 1:].split(",")[0] for r in rows]
# Load the input image
image = cv2.imread(args["image"])

'''
Resize image(s) to 224x224(GoogLeNet), 227x227(SqueezeNet),
pixels while performing mean subtraction (104, 117, 123) to normalize the input.
Blob will then be (1, 3, X, X), where X is dependent on network
'''
# Default is GoogLeNet dimensions
resize = cv2.resize(image, (224,224))
blob = cv2.dnn.blobFromImage(resize, 1, (224,224), (104,117,123))
if "squeeze" in args["model"]:
   resize = cv2.resize(image, (227,227))
   blob = cv2.dnn.blobFromImage(resize, 1, (227,227), (104,117,123))
if "mobile" in args["model"]:
   # Image already rezied from GoogLeNet default
   blob = cv2.dnn.blobFromImage(resize, 0.017, (224,224), (103.94,116.78,123.68))
if "alex" in args["model"]:
   resize = cv2.resize(image, (227,227))
   blob = cv2.dnn.blobFromImage(resize, 1, (227, 227), (103.94,116.78,123.68))

# Load serialized model
print("[INFO] loading model...")
loadStart = time.time()
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])
loadEnd = time.time()
# Set the blob as input to the network and perform a forward-pass 
# to obtain output classification
net.setInput(blob)
predStart = time.time()
predictions = net.forward()
predEnd = time.time()
print("[INFO] model load time: {:.5} seconds".format(loadEnd-loadStart))
print("[INFO] classification time: {:.5} seconds".format(predEnd-predStart))
totalTime = (loadEnd-loadStart) + (predEnd-predStart)
print("[INFO] total time: {:.5} seconds".format(totalTime))

# Sort the indexes of the probabilities in decending order (higher prob
# first) and grab top-5 predictions
predictions = predictions.reshape((1, len(classes)))
idxs = np.argsort(predictions[0])[::-1][:5]

# Display predictions
for(i, idx) in enumerate(idxs):
   # draw top prediction on input image
   if i == 0:
      text = "{}, {:.2f}%".format(classes[idx], predictions[0][idx]*100)

   # display predicted label + associated probability to the console
   print("[INFO] {}. label: {}, probability: {:.2f}%".format(i+1, classes[idx], predictions[0][idx]*100))

# Display output image
if args["display"] > 0:
   cv2.imshow(text, cv2.resize(image, (640,360)))
   cv2.waitKey(0)


