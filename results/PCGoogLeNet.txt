------- GOOGLENET -------
P 51 Test
[INFO] loading model...
[INFO] model load time: 0.048159 seconds
[INFO] classification time: 0.047908 seconds
[INFO] total time: 0.096067 seconds
[INFO] 1. label: warplane, probability: 44.38%
[INFO] 2. label: wing, probability: 44.07%
[INFO] 3. label: airliner, probability: 10.55%
[INFO] 4. label: aircraft carrier, probability: 0.45%
[INFO] 5. label: projectile, probability: 0.19%

Border Collie Test
[INFO] loading model...
[INFO] model load time: 0.046881 seconds
[INFO] classification time: 0.046386 seconds
[INFO] total time: 0.093268 seconds
[INFO] 1. label: Border collie, probability: 91.69%
[INFO] 2. label: collie, probability: 6.58%
[INFO] 3. label: Shetland sheepdog, probability: 0.64%
[INFO] 4. label: Appenzeller, probability: 0.31%
[INFO] 5. label: kelpie, probability: 0.10%

Kitchen Test
[INFO] loading model...
[INFO] model load time: 0.046059 seconds
[INFO] classification time: 0.04886 seconds
[INFO] total time: 0.094919 seconds
[INFO] 1. label: restaurant, probability: 14.71%
[INFO] 2. label: library, probability: 9.89%
[INFO] 3. label: sliding door, probability: 8.92%
[INFO] 4. label: patio, probability: 8.24%
[INFO] 5. label: bannister, probability: 8.07%

Diver Test
[INFO] loading model...
[INFO] model load time: 0.045997 seconds
[INFO] classification time: 0.048192 seconds
[INFO] total time: 0.094188 seconds
[INFO] 1. label: scuba diver, probability: 98.24%
[INFO] 2. label: snorkel, probability: 1.13%
[INFO] 3. label: hammerhead, probability: 0.26%
[INFO] 4. label: tiger shark, probability: 0.22%
[INFO] 5. label: wreck, probability: 0.04%
