------- MOBILENET -------
P 51 Test
[INFO] loading model...
[INFO] model load time: 0.019489 seconds
[INFO] classification time: 0.051932 seconds
[INFO] total time: 0.071421 seconds
[INFO] 1. label: airliner, probability: 47.98%
[INFO] 2. label: space shuttle, probability: 19.34%
[INFO] 3. label: warplane, probability: 14.68%
[INFO] 4. label: wing, probability: 10.45%
[INFO] 5. label: airship, probability: 3.09%

Border Collie Test
[INFO] loading model...
[INFO] model load time: 0.019096 seconds
[INFO] classification time: 0.053239 seconds
[INFO] total time: 0.072335 seconds
[INFO] 1. label: Border collie, probability: 55.03%
[INFO] 2. label: collie, probability: 7.16%
[INFO] 3. label: groenendael, probability: 4.45%
[INFO] 4. label: vulture, probability: 3.61%
[INFO] 5. label: prairie chicken, probability: 3.41%

Kitchen Test
[INFO] loading model...
[INFO] model load time: 0.019633 seconds
[INFO] classification time: 0.050894 seconds
[INFO] total time: 0.070527 seconds
[INFO] 1. label: patio, probability: 26.45%
[INFO] 2. label: prison, probability: 12.39%
[INFO] 3. label: sliding door, probability: 7.53%
[INFO] 4. label: church, probability: 5.63%
[INFO] 5. label: restaurant, probability: 5.59%

Diver Test
[INFO] loading model...
[INFO] model load time: 0.019668 seconds
[INFO] classification time: 0.051773 seconds
[INFO] total time: 0.071441 seconds
[INFO] 1. label: scuba diver, probability: 99.27%
[INFO] 2. label: hammerhead, probability: 0.22%
[INFO] 3. label: tiger shark, probability: 0.14%
[INFO] 4. label: fountain, probability: 0.10%
[INFO] 5. label: wreck, probability: 0.04%
