------- MOBILENET -------
P 51 Test
[INFO] loading model...
[INFO] model load time: 0.19122 seconds
[INFO] classification time: 1.2281 seconds
[INFO] total time: 1.4194 seconds
[INFO] 1. label: airliner, probability: 47.98%
[INFO] 2. label: space shuttle, probability: 19.34%
[INFO] 3. label: warplane, probability: 14.68%
[INFO] 4. label: wing, probability: 10.45%
[INFO] 5. label: airship, probability: 3.09%

Border Collie Test
[INFO] loading model...
[INFO] model load time: 0.19416 seconds
[INFO] classification time: 1.2183 seconds
[INFO] total time: 1.4125 seconds
[INFO] 1. label: Border collie, probability: 55.03%
[INFO] 2. label: collie, probability: 7.16%
[INFO] 3. label: groenendael, probability: 4.45%
[INFO] 4. label: vulture, probability: 3.61%
[INFO] 5. label: prairie chicken, probability: 3.41%

Kitchen Test
[INFO] loading model...
[INFO] model load time: 0.18958 seconds
[INFO] classification time: 1.2258 seconds
[INFO] total time: 1.4153 seconds
[INFO] 1. label: patio, probability: 26.45%
[INFO] 2. label: prison, probability: 12.39%
[INFO] 3. label: sliding door, probability: 7.53%
[INFO] 4. label: church, probability: 5.63%
[INFO] 5. label: restaurant, probability: 5.59%

Diver Test
[INFO] loading model...
[INFO] model load time: 0.18691 seconds
[INFO] classification time: 1.2096 seconds
[INFO] total time: 1.3965 seconds
[INFO] 1. label: scuba diver, probability: 99.27%
[INFO] 2. label: hammerhead, probability: 0.22%
[INFO] 3. label: tiger shark, probability: 0.14%
[INFO] 4. label: fountain, probability: 0.10%
[INFO] 5. label: wreck, probability: 0.04%
